--matched data

select a.date_transaction,
	b.business_day_format,
	right(concat('0000',a.store_code),4)  as store_code_inf_fact,
	b.store_code as store_code_bigquery,
	a.total_sales_qty,
	b.total_qty as total_qty_bigquery
from public.fact_total_sales_qty a 
left join public.fact_total_qty_bigquery_summary b on a.date_transaction = b.business_day_format
	and right(concat('0000',a.store_code),4)  = b.store_code
where a.total_sales_qty = b.total_qty